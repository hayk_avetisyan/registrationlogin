package com.egs.servlets;

import com.egs.dbService.DB_Service;
import com.egs.model.User;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

public class RegisterServlet extends HttpServlet {
    @Override
    public void init() throws ServletException {

    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {

        DB_Service db_service = DB_Service.getDb_service();
        User user=new User(request.getParameter("name"), request.getParameter("surname"), request.getParameter("login"),request.getParameter("password") );
        db_service.addUser(user);
        RequestDispatcher rd = getServletContext().getRequestDispatcher("/index.jsp");
        PrintWriter out= response.getWriter();
        out.println("<font color=green>Successfully registered.</font>");
        rd.include(request, response);
        db_service.getAllUsers();
    }

        @Override
    public void destroy() {

    }
}
