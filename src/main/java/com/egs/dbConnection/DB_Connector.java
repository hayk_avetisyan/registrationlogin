package com.egs.dbConnection;

import com.mongodb.MongoClient;
import com.mongodb.MongoClientURI;


public class DB_Connector {
    private static MongoClient mongoClient = null;

    public static MongoClient connect(){
        if(mongoClient == null){
            mongoClient = new MongoClient();
        }
        return mongoClient;
    }



}
