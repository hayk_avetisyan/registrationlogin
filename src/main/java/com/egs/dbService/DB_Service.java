package com.egs.dbService;

import com.egs.dbConnection.DB_Connector;
import com.egs.model.User;
import com.mongodb.BasicDBObject;
import com.mongodb.DBCollection;
import com.mongodb.DBCursor;
import com.mongodb.DBObject;
import com.mongodb.client.ClientSession;
import com.mongodb.client.FindIterable;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import org.bson.Document;

import java.util.ArrayList;
import java.util.List;


public class DB_Service {
    private MongoDatabase database;
    private static DB_Service db_service;

    private DB_Service(){
        this.database = DB_Connector.connect().getDatabase("auth");

    }
public static DB_Service getDb_service(){
        if(db_service==null){
            return new DB_Service();
        }
        return db_service;
}
    public void getAllUsers(){
        MongoCollection<BasicDBObject> collection = database.getCollection("users", BasicDBObject.class);
        List<BasicDBObject> foundDocument = collection.find().into(new ArrayList<BasicDBObject>());
        System.out.println(foundDocument);
    }
public void addUser(User user){
    MongoCollection<BasicDBObject> collection =  database.getCollection("users", BasicDBObject.class);
    BasicDBObject document = new BasicDBObject();
    document.put("name", ""+user.getName());
    document.put("surname", ""+user.getSurname());
    document.put("login", ""+user.getLogin());
    document.put("password",""+user.getPassword());
    collection.insertOne(document);
}

public String getPassword(String login){
    MongoCollection<BasicDBObject> collection =  database.getCollection("users", BasicDBObject.class);
    Document searchQuery = new Document();
    searchQuery.put("login", login);

    FindIterable<BasicDBObject> documents = collection.find(searchQuery);
    String password=null;
    for (BasicDBObject document: documents) {
 password= (String) document.get("password");
    }
return password;
}

}
